package com.gyosida.hiring.smartbox.matches

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gyosida.hiring.smartbox.R
import com.gyosida.hiring.smartbox.data.entities.Match
import com.gyosida.hiring.smartbox.extensions.formattedStart
import kotlinx.android.synthetic.main.layout_match_even.view.*

class MatchesAdapter : RecyclerView.Adapter<MatchesAdapter.MatchViewHolder>() {

    private val matchesList = mutableListOf<Match>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MatchViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(obtainLayoutForViewType(viewType), viewGroup, false)
        return MatchViewHolder(view)
    }

    private fun obtainLayoutForViewType(viewType: Int): Int = when(viewType) {
        EVEN_TYPE -> R.layout.layout_match_even
        else -> R.layout.layout_match_odd
    }

    override fun getItemCount(): Int = matchesList.size

    override fun getItemViewType(position: Int): Int {
        return if (position % 2 == 0) EVEN_TYPE else ODD_TYPE
    }

    override fun onBindViewHolder(viewHolder: MatchViewHolder, position: Int) {
        val match = matchesList[position]
        with(match) {
            viewHolder.homeTeamTV.text = homeTeam.name
            viewHolder.awayTeamTV.text = awayTeam.name
            viewHolder.homeScoreTV.updateScoreTypeface(homeScore, awayScore)
            viewHolder.homeScoreTV.text = homeScore.toString()
            viewHolder.awayScoreTV.text = awayScore.toString()
            viewHolder.awayScoreTV.updateScoreTypeface(awayScore, homeScore)
            viewHolder.matchStatusTV.text = eventStatus.name.es
            viewHolder.matchDateTV.text = matchDay.formattedStart
        }
    }

    private fun TextView.updateScoreTypeface(score: Int, otherScore: Int) {
        setTypeface(typeface, if (score > otherScore) Typeface.BOLD else Typeface.NORMAL)
    }

    fun setMatches(matches: List<Match>) {
        matchesList.clear()
        matchesList.addAll(matches)
        notifyDataSetChanged()
    }

    class MatchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val homeTeamTV: TextView = itemView.homeTeamTV
        val awayTeamTV: TextView = itemView.awayTeamTV
        val homeScoreTV: TextView = itemView.homeScoreTV
        val awayScoreTV: TextView = itemView.awayScoreTV
        val matchStatusTV: TextView = itemView.statusTV
        val matchDateTV: TextView = itemView.matchDateTV
    }

    companion object {
        private const val EVEN_TYPE = 1
        private const val ODD_TYPE = 2
    }
}
