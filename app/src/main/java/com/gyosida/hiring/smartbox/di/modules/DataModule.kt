package com.gyosida.hiring.smartbox.di.modules

import com.gyosida.hiring.smartbox.data.BuildConfig
import com.gyosida.hiring.smartbox.data.CredentialRepository
import com.gyosida.hiring.smartbox.data.MatchesRepository
import com.gyosida.hiring.smartbox.data.device.ClientConfigProvider
import com.gyosida.hiring.smartbox.data.local.database.SportsDatabase
import com.gyosida.hiring.smartbox.data.local.keyvalue.KeyValueStorage
import com.gyosida.hiring.smartbox.data.remote.AuthenticationAPI
import com.gyosida.hiring.smartbox.data.remote.MatchApi
import com.gyosida.hiring.smartbox.data.remote.http.HttpClient
import com.gyosida.hiring.smartbox.data.security.Authenticator

open class DataModule {

    private var _credentialRepository: CredentialRepository? = null

    @Synchronized
    open fun provideCredentialRepository(keyValueStorage: KeyValueStorage) =
        _credentialRepository ?: CredentialRepository(keyValueStorage).apply {
            _credentialRepository = this
        }

    open fun provideAuthenticator(httpClient: HttpClient, clientConfigProvider: ClientConfigProvider): Authenticator =
            Authenticator(
                    httpClient.create(AuthenticationAPI::class.java),
                    clientConfigProvider,
                    BuildConfig.AUTHENTICATION_KEY
            )

    open fun provideMatchesRepository(httpClient: HttpClient, sportsDatabase: SportsDatabase): MatchesRepository =
            MatchesRepository(
                    sportsDatabase.matchDao(),
                    httpClient.createAuthenticated(MatchApi::class.java)
            )
}
