package com.gyosida.hiring.smartbox

import android.app.Application
import com.gyosida.hiring.smartbox.di.Injection
import com.gyosida.hiring.smartbox.di.modules.*

class SportMatchesApp : Application() {

    override fun onCreate() {
        super.onCreate()

        Injection.initialize(
                ApplicationModule(this),
                DataModule(),
                DomainModule(),
                HttpModule(BuildConfig.BASE_URL),
                PersistenceModule()
        )
    }
}
