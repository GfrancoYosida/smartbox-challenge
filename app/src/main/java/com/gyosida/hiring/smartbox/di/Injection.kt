package com.gyosida.hiring.smartbox.di

import com.gyosida.hiring.smartbox.di.modules.*
import com.gyosida.hiring.smartbox.domain.GetMatchesInteractor

object Injection {

    private lateinit var applicationModule: ApplicationModule
    private lateinit var dataModule: DataModule
    private lateinit var domainModule: DomainModule
    private lateinit var httpModule: HttpModule
    private lateinit var persistenceModule: PersistenceModule

    fun initialize(
            applicationModule: ApplicationModule,
            dataModule: DataModule,
            domainModule: DomainModule,
            httpModule: HttpModule,
            persistenceModule: PersistenceModule
    ) {
        this.applicationModule = applicationModule
        this.dataModule = dataModule
        this.domainModule = domainModule
        this.httpModule = httpModule
        this.persistenceModule = persistenceModule
    }

    fun injectGetMatchesInteractor(): GetMatchesInteractor {
        val applicationContext = applicationModule.provideApplicationContext()
        val credentialRepository = dataModule.provideCredentialRepository(persistenceModule.provideKeyValueStorage(applicationContext))
        val httpClient = httpModule.provideHttpClient(httpModule.provideAuthenticationInterceptor(credentialRepository))
        val authenticator = dataModule.provideAuthenticator(httpClient, applicationModule.provideClientConfigProvider())
        val matchesRepository = dataModule.provideMatchesRepository(httpClient, persistenceModule.provideSportsDatabase(applicationContext))

        return domainModule.provideGetMatchesInteractor(authenticator, credentialRepository, matchesRepository)
    }
}
