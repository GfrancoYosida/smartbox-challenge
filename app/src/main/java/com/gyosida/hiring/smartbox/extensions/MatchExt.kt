package com.gyosida.hiring.smartbox.extensions

import android.content.Context
import android.text.format.DateFormat
import com.gyosida.hiring.smartbox.data.entities.Match
import java.text.SimpleDateFormat
import java.util.*

val Match.Day.formattedStart: String
    get() {
        val sdfParser = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'", Locale("ES"))
        val sdfFormatter = SimpleDateFormat("dd-MM-yyyy hh:mm", Locale("ES"))
        return sdfFormatter.format(sdfParser.parse(start))
    }
