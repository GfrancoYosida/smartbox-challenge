package com.gyosida.hiring.smartbox.di.modules

import com.gyosida.hiring.smartbox.data.CredentialRepository
import com.gyosida.hiring.smartbox.data.remote.http.HttpClient
import com.gyosida.hiring.smartbox.data.remote.http.interceptor.AuthenticationInterceptor

open class HttpModule(private val baseUrl: String) {

    private var httpClient: HttpClient? = null

    open fun provideAuthenticationInterceptor(credentialRepository: CredentialRepository): AuthenticationInterceptor =
            AuthenticationInterceptor(credentialRepository)

    @Synchronized
    open fun provideHttpClient(authenticationInterceptor: AuthenticationInterceptor): HttpClient =
            httpClient ?: HttpClient(authenticationInterceptor, baseUrl).apply {
                httpClient = this
            }
}
