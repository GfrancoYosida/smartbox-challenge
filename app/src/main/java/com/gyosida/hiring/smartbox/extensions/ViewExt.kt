package com.gyosida.hiring.smartbox.extensions

import android.view.View

fun Boolean.toVisibleOrGone() = if (this) View.VISIBLE else View.GONE
