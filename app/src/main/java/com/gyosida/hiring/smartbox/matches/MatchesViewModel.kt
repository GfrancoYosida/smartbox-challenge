package com.gyosida.hiring.smartbox.matches

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.gyosida.hiring.smartbox.data.entities.Match
import com.gyosida.hiring.smartbox.domain.GetMatchesInteractor
import com.gyosida.smartbox.core.exception.SmartBoxException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MatchesViewModel(private val getMatchesInteractor: GetMatchesInteractor) : ViewModel() {

    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)
    private val _loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    private val _emptyStateVisibleLiveData: MutableLiveData<Boolean> = MutableLiveData()
    private val _displayErrorLiveData: MutableLiveData<String> = MutableLiveData()

    private lateinit var _matchesLiveData: MutableLiveData<List<Match>>

    val refreshingLiveData: LiveData<Boolean>
        get() = _loadingLiveData
    val emptyStateVisibleLiveData: LiveData<Boolean>
        get() = _emptyStateVisibleLiveData
    val displayErrorLiveData: LiveData<String>
        get() = _displayErrorLiveData
    val matchesLiveData: LiveData<List<Match>>
        get() {
            if (::_matchesLiveData.isInitialized.not()) {
                _matchesLiveData = MutableLiveData()
                fetchMatches()
            }
            return _matchesLiveData
        }

    private fun fetchMatches() {
        scope.launch {
            _loadingLiveData.postValue(true)
            try {
                val matches = getMatchesInteractor.getMatches()
                if (matches.isEmpty()) _emptyStateVisibleLiveData.postValue(true)
                else _matchesLiveData.postValue(matches)
            } catch (exception: SmartBoxException) {
                _displayErrorLiveData.postValue(createErrorMessageFrom(exception))
            } finally {
                _loadingLiveData.postValue(false)
            }
        }
    }

    private fun createErrorMessageFrom(exception: SmartBoxException) =
            "${exception.description}\n${exception.recoverySuggestion}"

    fun refreshMatches() {
        _emptyStateVisibleLiveData.postValue(false)
        fetchMatches()
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}
