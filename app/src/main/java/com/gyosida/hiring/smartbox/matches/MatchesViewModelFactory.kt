package com.gyosida.hiring.smartbox.matches

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.gyosida.hiring.smartbox.domain.GetMatchesInteractor

class MatchesViewModelFactory(private val getMatchesInteractor: GetMatchesInteractor) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MatchesViewModel(getMatchesInteractor) as T
    }
}
