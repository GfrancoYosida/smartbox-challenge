package com.gyosida.hiring.smartbox.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.gyosida.hiring.smartbox.data.local.database.SportsDatabase
import com.gyosida.hiring.smartbox.data.local.keyvalue.KeyValuePreferences
import com.gyosida.hiring.smartbox.data.local.keyvalue.KeyValueStorage
import com.gyosida.smartbox.core.parsing.GsonTransformer

open class PersistenceModule {

    private var sportsDatabase: SportsDatabase? = null

    open fun provideKeyValueStorage(context: Context): KeyValueStorage =
            KeyValuePreferences(
                    context.getSharedPreferences("smartbox_challenge", Context.MODE_PRIVATE),
                    GsonTransformer()
            )

    @Synchronized
    open fun provideSportsDatabase(context: Context): SportsDatabase =
            sportsDatabase ?: Room.databaseBuilder(
                    context,
                    SportsDatabase::class.java,
                    "sports-database").build().apply { sportsDatabase = this }
}
