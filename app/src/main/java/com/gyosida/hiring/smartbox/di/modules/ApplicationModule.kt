package com.gyosida.hiring.smartbox.di.modules

import android.content.Context
import com.gyosida.hiring.smartbox.data.device.ClientConfigProvider

class ApplicationModule(private val context: Context) {

    fun provideApplicationContext(): Context = context

    fun provideClientConfigProvider(): ClientConfigProvider =
            ClientConfigProvider(provideApplicationContext(), "1.0.0")
}
