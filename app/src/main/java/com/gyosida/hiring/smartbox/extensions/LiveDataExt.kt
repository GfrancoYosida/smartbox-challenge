package com.gyosida.hiring.smartbox.extensions

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer

fun <T> LiveData<T>.nonNullObserve(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(owner, Observer { value -> value?.let { observer(it) } })
}
