package com.gyosida.hiring.smartbox.di.modules

import com.gyosida.hiring.smartbox.data.CredentialRepository
import com.gyosida.hiring.smartbox.data.MatchesRepository
import com.gyosida.hiring.smartbox.data.security.Authenticator
import com.gyosida.hiring.smartbox.domain.GetMatchesInteractor

open class DomainModule {

    open fun provideGetMatchesInteractor(
            authenticator: Authenticator,
            credentialRepository: CredentialRepository,
            matchesRepository: MatchesRepository
    ): GetMatchesInteractor = GetMatchesInteractor(authenticator, credentialRepository, matchesRepository)
}
