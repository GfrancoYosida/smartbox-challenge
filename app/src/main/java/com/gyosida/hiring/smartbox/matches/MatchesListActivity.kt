package com.gyosida.hiring.smartbox.matches

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.gyosida.hiring.smartbox.R
import com.gyosida.hiring.smartbox.di.Injection
import com.gyosida.hiring.smartbox.extensions.nonNullObserve
import com.gyosida.hiring.smartbox.extensions.toVisibleOrGone
import kotlinx.android.synthetic.main.activity_matches_list.*

class MatchesListActivity : AppCompatActivity() {

    private val matchesAdapter = MatchesAdapter()

    private lateinit var emptyStateView: View
    private lateinit var matchesViewModel: MatchesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_matches_list)

        refreshMatchesSRL.setOnRefreshListener { matchesViewModel.refreshMatches() }

        matchesRV.adapter = matchesAdapter
        matchesRV.layoutManager = LinearLayoutManager(this)

        matchesViewModel = ViewModelProviders.of(this, MatchesViewModelFactory(Injection.injectGetMatchesInteractor()))
                .get(MatchesViewModel::class.java)

        observeMatches()
        observeRefreshingState()
        observeEmptyViewVisibility()
        observeDisplayError()
    }

    private fun observeMatches() {
        matchesViewModel.matchesLiveData.nonNullObserve(this) { matches ->
            matchesAdapter.setMatches(matches)
        }
    }

    private fun observeRefreshingState() {
        matchesViewModel.refreshingLiveData.nonNullObserve(this) { refreshing ->
                    refreshMatchesSRL.isRefreshing = refreshing
                }
    }

    private fun observeEmptyViewVisibility() {
        matchesViewModel.emptyStateVisibleLiveData.nonNullObserve(this) { visible ->
            if (::emptyStateView.isInitialized.not()) {
                emptyStateView = emptyStateVS.inflate()
            }
            emptyStateView.visibility = visible.toVisibleOrGone()
        }
    }

    private fun observeDisplayError() {
        matchesViewModel.displayErrorLiveData.nonNullObserve(this) { error ->
            Snackbar.make(containerCL, error, Snackbar.LENGTH_LONG).show()
        }
    }
}
