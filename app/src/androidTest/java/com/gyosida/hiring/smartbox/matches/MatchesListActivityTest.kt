package com.gyosida.hiring.smartbox.matches

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import com.gyosida.hiring.smartbox.R
import com.gyosida.hiring.smartbox.asserts.RecyclerViewItemCountAssertion
import com.gyosida.hiring.smartbox.mock.CredentialMockProvider
import com.gyosida.hiring.smartbox.mock.MatchMockProvider
import com.gyosida.hiring.smartbox.rules.AppStateRule
import com.squareup.okhttp.mockwebserver.MockResponse
import com.squareup.okhttp.mockwebserver.MockWebServer
import org.junit.Rule
import org.junit.Test

class MatchesListActivityTest {

    private val mockWebServer = MockWebServer()

    @get:Rule
    val activityRule = ActivityTestRule(MatchesListActivity::class.java, false, false)

    @get:Rule
    val appStateRule = AppStateRule(mockWebServer)

    @Test
    fun testEmptyViewIsDisplayedWhenNoMatches() {
        mockWebServer.enqueue(MockResponse().setResponseCode(200).setBody(CredentialMockProvider.mockCredentialJson()))
        mockWebServer.enqueue(MockResponse().setResponseCode(200).setBody(MatchMockProvider.mockEmptyMatchesListJson()))

        activityRule.launchActivity(null)
        onView(withText("No encontramos juegos recientes. Inténtalo nuevamente.")).check(matches(isDisplayed()))
    }

    @Test
    fun testMessageErrorDisplayedWhenCannotAuthenticate() {
        mockWebServer.enqueue(MockResponse().setResponseCode(400))

        activityRule.launchActivity(null)
        onView(withText("Ocurrió algo inesperado.\nPor favor, intenta más tarde la operación.")).check(matches(isDisplayed()))
    }

    @Test
    fun testMatchesAreDisplayed() {
        mockWebServer.enqueue(MockResponse().setResponseCode(200).setBody(CredentialMockProvider.mockCredentialJson()))
        mockWebServer.enqueue(MockResponse().setResponseCode(200).setBody(MatchMockProvider.mockMatchesListJson()))

        activityRule.launchActivity(null)
        onView(withId(R.id.matchesRV)).check(RecyclerViewItemCountAssertion(10))
    }
}
