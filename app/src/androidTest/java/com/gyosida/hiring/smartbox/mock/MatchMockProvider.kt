package com.gyosida.hiring.smartbox.mock

import com.gyosida.hiring.smartbox.data.remote.MatchApi
import com.gyosida.smartbox.core.test.MockProvider

object MatchMockProvider : MockProvider() {

    override val folder: String = "match"

    fun mockMatchesList(): MatchApi.MatchesList = parse("matches_list.json")

    fun mockMatchesListJson(): String = read("matches_list.json")

    fun mockEmptyMatchesListJson() = read("empty_matches_list.json")

    fun mockEmptyMatchesList(): MatchApi.MatchesList = parse("empty_matches_list.json")
}
