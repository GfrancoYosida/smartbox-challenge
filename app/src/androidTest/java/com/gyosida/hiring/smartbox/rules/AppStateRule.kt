package com.gyosida.hiring.smartbox.rules

import android.content.Context
import android.support.test.InstrumentationRegistry
import com.gyosida.hiring.smartbox.di.Injection
import com.gyosida.hiring.smartbox.di.modules.*
import com.squareup.okhttp.mockwebserver.MockWebServer
import org.junit.rules.ExternalResource

class AppStateRule(private val mockWebServer: MockWebServer) : ExternalResource() {

    private val persistenceModule = PersistenceModule()
    private lateinit var context: Context

    override fun before() {
        super.before()
        context = InstrumentationRegistry.getTargetContext().applicationContext
        mockWebServer.play()
        Injection.initialize(
                ApplicationModule(context),
                DataModule(),
                DomainModule(),
                HttpModule(mockWebServer.getUrl("/").toString()),
                persistenceModule
        )
    }

    override fun after() {
        super.after()
        mockWebServer.shutdown()
        persistenceModule.provideKeyValueStorage(context).clearAll()
        persistenceModule.provideSportsDatabase(context).clearAllTables()
    }
}
