package com.gyosida.hiring.smartbox.domain.mock

import com.gyosida.hiring.smartbox.data.entities.Credential
import com.gyosida.smartbox.core.test.MockProvider

object CredentialMockProvider :  MockProvider() {

    override val folder: String = "credential"

    fun mockCredential(): Credential = parse("credential.json")
}
