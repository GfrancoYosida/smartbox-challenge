package com.gyosida.hiring.smartbox.domain

import com.gyosida.hiring.smartbox.data.CredentialRepository
import com.gyosida.hiring.smartbox.data.MatchesRepository
import com.gyosida.hiring.smartbox.data.security.Authenticator
import com.gyosida.hiring.smartbox.domain.mock.CredentialMockProvider
import com.gyosida.hiring.smartbox.domain.mock.MatchMockProvider
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Test

class GetMatchesInteractorTest {

    private val authenticator = mock<Authenticator>()
    private val credentialRepository = mock<CredentialRepository>()
    private val matchesRepository = mock<MatchesRepository>()
    private val getMatchesInteractor = GetMatchesInteractor(authenticator, credentialRepository, matchesRepository)

    @Test
    fun `testGetMatchesShouldAuthenticateIfCredentialNotCached`() {
        val credential = CredentialMockProvider.mockCredential()
        val matches = MatchMockProvider.mockMatchesList().data.items
        whenever(credentialRepository.getCredential()).thenReturn(null)
        whenever(authenticator.authenticate()).thenReturn(credential)
        whenever(matchesRepository.getMatches()).thenReturn(matches)

        runBlocking {
            assertEquals(matches, getMatchesInteractor.getMatches())
            verify(credentialRepository).saveCredential(credential)
        }
    }
}
