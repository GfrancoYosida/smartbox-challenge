package com.gyosida.hiring.smartbox.domain.mock

import com.gyosida.hiring.smartbox.data.remote.MatchApi
import com.gyosida.smartbox.core.test.MockProvider

object MatchMockProvider : MockProvider() {

    override val folder: String = "match"

    fun mockMatchesList(): MatchApi.MatchesList = parse("matches_list.json")
}
