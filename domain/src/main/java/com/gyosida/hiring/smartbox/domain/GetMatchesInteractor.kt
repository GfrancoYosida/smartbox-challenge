package com.gyosida.hiring.smartbox.domain

import com.gyosida.hiring.smartbox.data.CredentialRepository
import com.gyosida.hiring.smartbox.data.entities.Match
import com.gyosida.hiring.smartbox.data.MatchesRepository
import com.gyosida.hiring.smartbox.data.security.Authenticator
import com.gyosida.hiring.smartbox.domain.exception.CannotAuthenticateException
import com.gyosida.smartbox.core.exception.SmartBoxException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetMatchesInteractor(
        private val authenticator: Authenticator,
        private val credentialRepository: CredentialRepository,
        private val matchesRepository: MatchesRepository
) {

    suspend fun getMatches(): List<Match> {
        return withContext(Dispatchers.IO) {
            if (credentialRepository.getCredential() == null) {
                try {
                    val credential = authenticator.authenticate()
                    credentialRepository.saveCredential(credential)
                } catch (connectivity: SmartBoxException.Connectivity) {
                    throw CannotAuthenticateException
                }
            }
            matchesRepository.getMatches()
        }
    }
}
