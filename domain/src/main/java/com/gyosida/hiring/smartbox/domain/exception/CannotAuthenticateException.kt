package com.gyosida.hiring.smartbox.domain.exception

import com.gyosida.smartbox.core.exception.SmartBoxException

object CannotAuthenticateException : SmartBoxException() {

    override val description: String = "No ha sido posible realizar la autenticación."
    override val recoverySuggestion: String = "Por favor, inténtalo nuevamente más tarde con conexión a internet"
}
