package com.gyosida.hiring.smartbox.data

import com.gyosida.hiring.smartbox.data.entities.Credential
import com.gyosida.hiring.smartbox.data.helpers.buildCredential
import com.gyosida.hiring.smartbox.data.local.keyvalue.KeyValueStorage
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString

class CredentialRepositoryTest {

    private val keyValueStorage = mock<KeyValueStorage>()
    private val credentialRepository = CredentialRepository(keyValueStorage)

    @Test
    fun `testSaveCredentialShouldPersistCredentialAsJson`() {
        val credential = buildCredential()
        credentialRepository.saveCredential(credential)

        verify(keyValueStorage).saveAsJson(anyString(), eq(buildCredential()))
    }

    @Test
    fun `testGetCredentialRetrieveInfoFromDisk`() {
        credentialRepository.getCredential()

        verify(keyValueStorage).getFromJson(anyString(), eq(Credential::class.java))
    }
}
