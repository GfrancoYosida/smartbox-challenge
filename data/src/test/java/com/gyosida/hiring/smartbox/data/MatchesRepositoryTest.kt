package com.gyosida.hiring.smartbox.data

import com.gyosida.hiring.smartbox.data.local.database.MatchDao
import com.gyosida.hiring.smartbox.data.mock.MatchMockProvider
import com.gyosida.hiring.smartbox.data.remote.MatchApi
import com.gyosida.smartbox.core.exception.SmartBoxException
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Test
import retrofit2.Call
import retrofit2.Response

class MatchesRepositoryTest {

    private val matchDao = mock<MatchDao>()
    private val matchApi = mock<MatchApi>()
    private val callMock = mock<Call<MatchApi.MatchesList>>()
    private val matchesRepository = MatchesRepository(matchDao, matchApi)

    @Test(expected = SmartBoxException.Unexpected::class)
    fun `testGetMatchesThrowsIllegalStateExceptionWhenBodyResponseIsNull`() {
        whenever(callMock.execute()).thenReturn(Response.success<MatchApi.MatchesList>(200, null))
        whenever(matchApi.getMatchesList()).thenReturn(callMock)

        matchesRepository.getMatches()
    }

    @Test
    fun `testGetMatchesRetrieveFromApiAndPersistLocally`() {
        val matchesList = MatchMockProvider.mockMatchesList()
        whenever(callMock.execute()).thenReturn(Response.success(201, matchesList))
        whenever(matchApi.getMatchesList()).thenReturn(callMock)

        matchesRepository.getMatches()

        verify(matchDao).insertMatches(matchesList.data.items)
        verify(matchDao).getAllMatches()
    }

    @Test
    fun `testGetMatchesReturnCachedMatchesWhenConnectionFails`() {
        val matchesList = MatchMockProvider.mockMatchesList()
        whenever(callMock.execute()).thenReturn(Response.error(408, ResponseBody.create(MediaType.parse("text/plain"), "test")))
        whenever(matchApi.getMatchesList()).thenReturn(callMock)
        whenever(matchDao.getAllMatches()).thenReturn(matchesList.data.items)

        assertEquals(matchesList.data.items, matchesRepository.getMatches())
    }
}

