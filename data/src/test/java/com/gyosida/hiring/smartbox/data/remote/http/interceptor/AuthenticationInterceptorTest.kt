package com.gyosida.hiring.smartbox.data.remote.http.interceptor

import com.gyosida.hiring.smartbox.data.CredentialRepository
import com.gyosida.hiring.smartbox.data.entities.Credential
import com.gyosida.hiring.smartbox.data.helpers.buildCredential
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.squareup.okhttp.mockwebserver.MockResponse
import com.squareup.okhttp.mockwebserver.MockWebServer
import okhttp3.OkHttpClient
import okhttp3.Request
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test

class AuthenticationInterceptorTest {

    private val mockWebServer = MockWebServer()
    private val credentialRepository = mock<CredentialRepository>()
    private val httpClient = OkHttpClient.Builder()
            .addInterceptor(AuthenticationInterceptor(credentialRepository))
            .build()

    @Before
    fun setup() {
        mockWebServer.play()
        mockWebServer.enqueue(MockResponse())
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `testAuthorizationHeaderIsNullWhenCredentialIsNotCached`() {
        val request = Request.Builder().url(mockWebServer.getUrl("/")).build()
        whenever(credentialRepository.getCredential()).thenReturn(null)

        httpClient.newCall(request).execute()

        assertNull(mockWebServer.takeRequest().getHeader("Authorization"))
    }

    @Test
    fun `testAuthorizationHeaderContainsAccessTokenFromCredential`() {
        val request = Request.Builder().url(mockWebServer.getUrl("/")).build()
        val credential = buildCredential()
        whenever(credentialRepository.getCredential()).thenReturn(credential)

        httpClient.newCall(request).execute()

        assertEquals("Bearer ${credential.accessToken}", mockWebServer.takeRequest().getHeader("Authorization"))
    }
}
