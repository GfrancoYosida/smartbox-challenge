package com.gyosida.hiring.smartbox.data.local.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.gyosida.hiring.smartbox.data.entities.Match

@Database(entities = arrayOf(Match::class), version = 1)
abstract class SportsDatabase: RoomDatabase() {

    abstract fun matchDao(): MatchDao
}
