package com.gyosida.hiring.smartbox.data

import com.gyosida.hiring.smartbox.data.entities.Match
import com.gyosida.hiring.smartbox.data.local.database.MatchDao
import com.gyosida.hiring.smartbox.data.remote.MatchApi
import com.gyosida.hiring.smartbox.data.remote.http.request
import com.gyosida.smartbox.core.exception.SmartBoxException

class MatchesRepository(
        private val matchDao: MatchDao,
        private val matchApi: MatchApi
) {

    fun getMatches(): List<Match> =
            try {
                val matches = request(matchApi.getMatchesList()).data.items
                matchDao.insertMatches(matches)
                matchDao.getAllMatches()
            } catch (connectivityException: SmartBoxException.Connectivity) {
                matchDao.getAllMatches()
            }
}
