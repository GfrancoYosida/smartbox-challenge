package com.gyosida.hiring.smartbox.data.remote

import com.gyosida.hiring.smartbox.data.entities.Match
import retrofit2.Call
import retrofit2.http.GET

interface MatchApi {

    @GET("sport/events")
    fun getMatchesList(): Call<MatchesList>

    data class MatchesList(val data: Data) {
        data class Data(val items: List<Match> = emptyList())
    }
}
