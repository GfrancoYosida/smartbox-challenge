package com.gyosida.hiring.smartbox.data

import com.gyosida.hiring.smartbox.data.entities.Credential
import com.gyosida.hiring.smartbox.data.local.keyvalue.KeyValueStorage

class CredentialRepository(private val keyValueStorage: KeyValueStorage) {

    @Synchronized
    fun saveCredential(credential: Credential) {
        keyValueStorage.saveAsJson(CREDENTIAL_KEY, credential)
    }

    @Synchronized
    fun getCredential(): Credential? =
        keyValueStorage.getFromJson(CREDENTIAL_KEY, Credential::class.java)

    companion object {
        private const val CREDENTIAL_KEY = "credential"
    }
}
