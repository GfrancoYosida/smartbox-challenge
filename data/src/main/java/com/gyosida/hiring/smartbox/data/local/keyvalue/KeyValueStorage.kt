package com.gyosida.hiring.smartbox.data.local.keyvalue

interface KeyValueStorage {

    fun <T> saveAsJson(key: String, value: T): Boolean

    fun <T> getFromJson(key: String, clazz: Class<T>): T?

    fun clearAll()
}
