package com.gyosida.hiring.smartbox.data.entities

data class Credential(
        val accessToken: String,
        val refreshToken: String,
        val expiresIn: String,
        val tokenType: String
)
