package com.gyosida.hiring.smartbox.data.local.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.gyosida.hiring.smartbox.data.entities.Match

@Dao
interface MatchDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMatches(matches: List<Match>)

    @Query(value = "SELECT * FROM matches")
    fun getAllMatches(): List<Match>
}
