package com.gyosida.hiring.smartbox.data.remote

import com.gyosida.hiring.smartbox.data.entities.ClientConfig
import com.gyosida.hiring.smartbox.data.entities.Credential
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthenticationAPI {

    @POST("auth/users/login/anonymous")
    fun authenticate(
            @Header("Authorization") authorization: String,
            @Body clientConfig: ClientConfig
    ): Call<CredentialPayload>

    data class CredentialPayload(val data: Credential)
}
