package com.gyosida.hiring.smartbox.data.remote.http

import com.gyosida.hiring.smartbox.data.remote.http.interceptor.AuthenticationInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class HttpClient(
        private val authenticationInterceptor: AuthenticationInterceptor,
        private val baseUrl: String
) {

    private val INSTANCE = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(createBasicHttpClient())
            .baseUrl(baseUrl)
            .build()

    private fun createBasicHttpClient(): OkHttpClient =
            OkHttpClient.Builder()
                    .addNetworkInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                    .build()

    fun <T> createAuthenticated(api: Class<T>): T =
            INSTANCE.newBuilder()
                    .client(createBasicHttpClient().newBuilder()
                            .addInterceptor(authenticationInterceptor).build())
                    .build()
                    .create(api)

    fun <T> create(api: Class<T>): T = INSTANCE.create(api)
}
