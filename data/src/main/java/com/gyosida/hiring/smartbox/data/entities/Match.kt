package com.gyosida.hiring.smartbox.data.entities

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "matches")
data class Match(
        @SerializedName("_id") @PrimaryKey val id: String,
        @Embedded(prefix = "home_team_") val homeTeam: Team,
        @Embedded(prefix = "away_team_") val awayTeam: Team,
        val homeScore: Int,
        val awayScore: Int,
        @Embedded(prefix = "match_day_") val matchDay: Day,
        @Embedded(prefix = "status_") val eventStatus: Status
) {

    data class Day(
            val start: String
    )

    data class Status(
            val category: String,
            @Embedded(prefix = "status_name_") val name: Name
    ) {

        data class Name(
                val es: String,
                val original: String
        )
    }

    data class Team(
            val name: String,
            val shortName: String
    )
}
