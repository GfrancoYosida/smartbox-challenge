package com.gyosida.hiring.smartbox.data.device

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.provider.Settings
import android.provider.Settings.Secure.getString
import android.util.DisplayMetrics
import android.view.WindowManager
import com.gyosida.hiring.smartbox.data.entities.ClientConfig

class ClientConfigProvider(
        private val context: Context,
        private val version: String
) {

    private val deviceDisplayMetrics: DisplayMetrics =
            (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).let {
                val displayMetrics = DisplayMetrics()
                it.defaultDisplay.getMetrics(displayMetrics)
                displayMetrics
            }

    fun provideClientConfig(): ClientConfig =
            ClientConfig(
                    ClientConfig.User(ClientConfig.User.Profile(getDeviceLocale())),
                    ClientConfig.Device(
                            getDeviceId(),
                            Build.DEVICE,
                            Build.VERSION.SDK_INT.toString(),
                            deviceDisplayMetrics.widthPixels.toString(),
                            deviceDisplayMetrics.heightPixels.toString(),
                            Build.MODEL
                    ),
                    ClientConfig.Application(version)
            )

    private fun getDeviceLocale(): String =
            if (Build.VERSION.SDK_INT >= 24) context.resources.configuration.locales[0].language
            else context.resources.configuration.locale.language

    @SuppressLint("HardwareIds")
    private fun getDeviceId(): String =
            try {
                getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            } catch (e: Exception) {
                "not_available"
            }
}
