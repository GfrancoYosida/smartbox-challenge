package com.gyosida.hiring.smartbox.data.local.keyvalue

import android.content.SharedPreferences
import com.gyosida.smartbox.core.parsing.JSONTransformer

class KeyValuePreferences(
        private val preferences: SharedPreferences,
        private val jsonTransformer: JSONTransformer
): KeyValueStorage {

    override fun <T> saveAsJson(key: String, value: T): Boolean =
        preferences.edit().putString(key, jsonTransformer.to(value)).apply().let { true }

    override fun <T> getFromJson(key: String, clazz: Class<T>): T? {
        val json = preferences.getString(key, null)

        return json?.let { jsonTransformer.from(it, clazz) }
    }

    override fun clearAll() {
        preferences.edit().clear().apply()
    }
}
