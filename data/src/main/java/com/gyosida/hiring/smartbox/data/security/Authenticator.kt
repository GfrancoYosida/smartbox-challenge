package com.gyosida.hiring.smartbox.data.security

import com.gyosida.hiring.smartbox.data.entities.Credential
import com.gyosida.hiring.smartbox.data.device.ClientConfigProvider
import com.gyosida.hiring.smartbox.data.remote.AuthenticationAPI
import com.gyosida.hiring.smartbox.data.remote.http.request

class Authenticator(
        private val authenticationAPI: AuthenticationAPI,
        private val clientConfigProvider: ClientConfigProvider,
        private val authenticationKey: String
) {

    fun authenticate(): Credential =
            request(authenticationAPI.authenticate(
                    authenticationKey,
                    clientConfigProvider.provideClientConfig()
            )).data
}
