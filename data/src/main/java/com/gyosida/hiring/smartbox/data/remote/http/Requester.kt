package com.gyosida.hiring.smartbox.data.remote.http

import com.gyosida.smartbox.core.exception.FailedHttpResponse
import com.gyosida.smartbox.core.exception.SmartBoxException
import retrofit2.Call
import java.io.IOException

fun <T> request(call: Call<T>): T =
        try {
            val response = call.execute()
            if (response.isSuccessful) {
                response.body() ?: throw SmartBoxException.Unexpected
            } else {
                val failedHttpResponse = FailedHttpResponse(response.code(), response.message())
                throw SmartBoxException.createFrom(failedHttpResponse)
            }
        } catch (ioException: IOException) {
            throw SmartBoxException.createFrom(ioException)
        }
