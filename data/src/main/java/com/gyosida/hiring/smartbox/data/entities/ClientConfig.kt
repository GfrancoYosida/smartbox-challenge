package com.gyosida.hiring.smartbox.data.entities

data class ClientConfig(
        val user: User,
        val device: Device,
        val app: Application
) {

    data class Device(
            val deviceId: String,
            val name: String,
            val version: String,
            val width: String,
            val height: String,
            val model: String,
            val platform: String = "android"
    )

    data class User(val profile: Profile) {

        data class Profile(val language: String)
    }

    data class Application(val version: String)
}
