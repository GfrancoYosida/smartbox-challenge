package com.gyosida.hiring.smartbox.data.remote.http.interceptor

import android.util.Log
import com.gyosida.hiring.smartbox.data.entities.Credential
import com.gyosida.hiring.smartbox.data.CredentialRepository
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthenticationInterceptor(private val credentialRepository: CredentialRepository) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val credential = credentialRepository.getCredential()
        return if (credential == null) {
            System.out.println("AuthenticationInterceptor: no credential was found")
            chain.proceed(chain.request())
        } else chain.proceed(addAuthorizationHeader(chain.request(), credential))
    }

    private fun addAuthorizationHeader(request: Request, credential: Credential): Request =
            request.newBuilder()
                    .addHeader("Authorization", "Bearer ${credential.accessToken}")
                    .build()
}
