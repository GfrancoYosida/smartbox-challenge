package com.gyosida.smartbox.core.parsing

interface JSONTransformer {

    fun <T> from(json: String, clazz: Class<T>): T

    fun <T> to(model: T): String
}