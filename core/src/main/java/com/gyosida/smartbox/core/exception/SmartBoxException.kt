package com.gyosida.smartbox.core.exception

import java.io.IOException
import kotlin.Exception

abstract class SmartBoxException : Exception() {

    abstract val description: String
    abstract val recoverySuggestion: String

    object Connectivity : SmartBoxException() {
        override val description: String = "Estamos teniendo inconvenientes con la conexión a internet."
        override val recoverySuggestion: String = "Por favor, Intenta nuevamente la operación."
    }
    object Unexpected : SmartBoxException() {
        override val description: String = "Ocurrió algo inesperado."
        override val recoverySuggestion: String = "Por favor, intenta más tarde la operación."
    }

    companion object {
        private const val NO_INTERNET_CODE = 408
        private const val NO_RESPONSE_CODE = -1

        fun createFrom(failedHttpResponse: FailedHttpResponse): SmartBoxException =
                when(failedHttpResponse.code) {
                    NO_INTERNET_CODE, NO_RESPONSE_CODE -> Connectivity
                    else -> Unexpected
                }

        fun createFrom(ioException: IOException): SmartBoxException = Connectivity
    }
}
