package com.gyosida.smartbox.core.test

import com.google.gson.Gson

abstract class MockProvider {

    protected val gson = Gson()

    abstract val folder: String

    protected fun read(fileName: String): String {
        val input = MockProvider::class.java.getResourceAsStream("/$folder/$fileName")

        return input
                .bufferedReader()
                .use { it.readText() }
    }

    protected inline fun <reified T: Any> parse(resourceName: String): T {
        return gson.fromJson(read(resourceName), T::class.java)
    }

}
