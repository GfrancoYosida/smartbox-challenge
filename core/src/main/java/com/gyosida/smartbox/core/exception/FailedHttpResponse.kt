package com.gyosida.smartbox.core.exception

data class FailedHttpResponse(
        val code: Int,
        val message: String
)
