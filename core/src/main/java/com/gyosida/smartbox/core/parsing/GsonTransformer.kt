package com.gyosida.smartbox.core.parsing

import com.google.gson.Gson

class GsonTransformer: JSONTransformer {

    override fun <T> from(json: String, clazz: Class<T>): T {
        return Gson().fromJson(json, clazz)
    }

    override fun <T> to(model: T): String {
        return Gson().toJson(model)
    }
}