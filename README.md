# SmartBox Challenge #
Android application that display a list of matches.
![](smartbox-demo.gif)

## **Getting Started** ##
### Clone the repository ###
Clone the project at your local machine using the following command line at the terminal:
```
#!unix
git clone https://GfrancoYosida@bitbucket.org/GfrancoYosida/smartbox-challenge.git
```
you should be able to clone it since it is a public repository.
### Prerequisites ###
Have installed the following:

* Jdk 7.
* Android Studio 3.2.1
* Android SDK 28.
* Gradle build tools 3.1.4
* Android Emulator (Nexus 5x API 25 is a good one).

### Open and Run in Android Studio ###
1. Open project in Android Studio.
2. Gradle Build System will configure and install dependencies.
3. Connect your physical device or run an emulator instance.
3. Press the Run button.

### Running tests
Every module has a suite of unit tests and/or instrumented tests.
1. Go to the androidTest/test folder within the desired module.
2. Right click the main package and press Run Tests.
3. Enjoy seeing all passing :)

### **Author** ###
* Gianfranco Yosida

You can reach me out at gianfranco.yosida@gmail.com
